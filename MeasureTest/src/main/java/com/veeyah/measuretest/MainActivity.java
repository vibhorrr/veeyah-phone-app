package com.veeyah.measuretest;

import android.app.AlertDialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    private TextView home;
    private BroadcastReceiver listen;
    private AlertDialog msg = null;
    private boolean waitingOnUserInfo = false;

    @Override
    protected void onPause() {
        if (msg != null) {
            msg.dismiss();
            msg = null;
        }
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        VeeyahBattery.registerLocalBroadcast(listen, this);
        Intent loadData = new Intent(getApplicationContext(), VeeyahBattery.class);
        loadData.setAction(VeeyahBattery.REFRESH);
        startService(loadData);
    }

    @Override
    protected void onStop() {
        VeeyahBattery.unregister(listen);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        home = (TextView) findViewById(R.id.veeyahhome);

        listen = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    //Log.i("Veeyah", "Got broadcast: "+intent.getAction());
                    if (VeeyahBattery.REFRESH.equals(intent.getAction())) {
                        refreshView((MatchBoxModel) intent.getParcelableExtra(VeeyahBattery.MB_MODEL));
                    } else if (VeeyahBattery.GET_USER_INFO.equals(intent.getAction())) {
                        if (!waitingOnUserInfo) getUserInfo();
                    }
                } catch (Exception e) {
                    Log.w("Veeyah", "Exception receiving local broadcast '"+intent.getAction()+"' in MainActivity: "+e.getClass()+": "+e.getMessage());
                }
            }
        };

        VeeyahBattery.registerLocalBroadcast(listen, this);
        Log.v("Veeyah", "Receiver is listening. . .");

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        Intent loadData = new Intent(getApplicationContext(), VeeyahBattery.class);
        loadData.setAction(VeeyahBattery.REFRESH);
        startService(loadData);
    }

    //this method will be called if the backend thinks the device needs to be registered (since it cannot find a deviceID)
    protected void getUserInfo() {
        try {
            //this section creates a dialog to get the user's name and password
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Register your device:");

            final LinearLayout layout = new LinearLayout(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout.setLayoutParams(lp);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams tp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            final TextView nameLbl = new TextView(this);
            nameLbl.setText("Name:");
            final EditText nameTxt = new EditText(this);
            nameTxt.setInputType(InputType.TYPE_CLASS_TEXT);
            nameTxt.setLayoutParams(tp);
            final TextView emailLbl = new TextView(this);
            emailLbl.setText("\nEmail:");
            final EditText emailTxt = new EditText(this);
            emailTxt.setInputType(InputType.TYPE_CLASS_TEXT);
            emailTxt.setLayoutParams(tp);
            final TextView passLbl = new TextView(this);
            passLbl.setText("\nPassword:");
            final EditText passTxt = new EditText(this);
            passTxt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            passTxt.setLayoutParams(tp);
            layout.addView(nameLbl);
            layout.addView(nameTxt);
            layout.addView(emailLbl);
            layout.addView(emailTxt);
            layout.addView(passLbl);
            layout.addView(passTxt);

            builder.setView(layout);

            //this interface gets the user's name and password using the view created above
            DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String name = "";
                    String email = "";
                    String pass = "";
                    try {
                        if (nameTxt.getText() != null) name = nameTxt.getText().toString();
                        if (emailTxt.getText() != null) email = emailTxt.getText().toString();
                        if (passTxt.getText() != null) pass = passTxt.getText().toString();
                    } catch (Exception e) {
                        //do nothing
                    }

                    //this tells the controller to do the registration with this info
                    Intent register = new Intent(getApplicationContext(), VeeyahBattery.class);
                    register.putExtra(VeeyahBattery.USER_NAME, name);
                    register.putExtra(VeeyahBattery.USER_EMAIL, email);
                    register.putExtra(VeeyahBattery.PASSWORD, pass);
                    register.setAction(VeeyahBattery.REGISTER);
                    startService(register);
                    if (msg != null) {
                        msg.dismiss();
                        msg = null;
                    }
                }
            };

            builder.setPositiveButton("OK", ocl);
            this.msg = builder.create();
            msg.show();
            waitingOnUserInfo = true;
        } catch (Exception e) {
            Log.w("Veeyah", "Exception getting registration info from user: " + e.getClass() + ": " + e.getMessage());
        }
    }

    //this method will get called each time the battery updates with a fresh model to update the view
    protected void refreshView(MatchBoxModel mbm) {

        //NotificationManager nman = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //nman.notify(0, n);
        if (!"".equals(mbm.getDeviceID())) waitingOnUserInfo = false;
        String toDisp = "Monitoring Began At: \n\t"+mbm.getStartTime()+"\n\n";
        toDisp += "Last Update At: \n\t"+mbm.getLastTime()+"\n\n";


        toDisp += "Battery Level: "+mbm.getLastPoint()+"%\n";
        toDisp += "Battery Capacity: "+mbm.getCapacity()+" Wh\n";

        //toDisp += "Total Charge (Est): "+mbm.getTotCharge()+" mAh\n";
        //toDisp += "Total Discharge (Est): "+mbm.getTotDischarge()+" mAh\n";
        //note that SunJoules are modeled in hundredths and must be reformatted for display (see dispLong)
        toDisp += "SunJoules Used: "+dispLong(mbm.getUsedSJ())+"\n";
        toDisp += "SunJoules Available: "+dispLong(mbm.getAvailableSJ())+"\n";


        /*toDisp += "Battery Status: ";
        switch (batteryStatus) {
            case BatteryManager.BATTERY_STATUS_CHARGING:
                toDisp += "Charging";
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                toDisp += "Discharging";
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                toDisp += "Full";
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                toDisp += "Not Charging";
                break;
            default:
                toDisp += "Unknown";
                break;
        }
        toDisp += "\n";*/
        home.setText(toDisp);
    }

    private String dispLong(long l) {  //displays a long in hundredths with two decimals
        long dec = (l % 100);
        String padding = "";
        if (dec < 10) padding = "0";
        return((l/100)+"."+padding+dec);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_main, container, false);
        }
    }

}
