package com.veeyah.measuretest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Should start monitoring battery upon BOOT_COMPLETED event
 * Created by Williams on 12/11/13
 */
public class BootListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent myStarterIntent = new Intent(context, VeeyahBattery.class);
            myStarterIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            myStarterIntent.setAction(Intent.ACTION_BOOT_COMPLETED);
            context.startService(myStarterIntent);
        }
    }
}
