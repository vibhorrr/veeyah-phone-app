package com.veeyah.measuretest;


import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;

public class MatchBoxModel implements Parcelable {

    private String startTime, lastTime, deviceID;
    private int totCap, totCharge, totDischarge;
    private long sjBurn, sjAvail, lastPoint;  //these are in hundredths of units, so they should be divided by 100 for display
    private long idleTime;
    //private static final long serialVersionUID = 0L;
    private static final int DEFAULT_CAPACITY = 12;  //in watthours

    protected MatchBoxModel() {
        startTime = printTime();
        lastTime = startTime;
        totCharge = 0;
        totDischarge = 0;
        lastPoint = -1;
        totCap = getBatteryCap();
        sjBurn = 0;
        sjAvail = 0;
        idleTime = 0;
        deviceID = "";
    }

    protected MatchBoxModel(Parcel in) {
        Bundle b = in.readBundle();
        if (b == null) return;
        startTime = b.getString("startTime");
        if (startTime == null) startTime = printTime();
        lastTime = b.getString("lastTime");
        if (lastTime == null) lastTime = startTime;
        totCharge = b.getInt("totCharge", 0);
        totDischarge = b.getInt("totDischarge", 0);
        lastPoint = b.getLong("lastPoint", -1);
        totCap = b.getInt("totCap", getBatteryCap());
        sjBurn = b.getLong("sjBurn", 0);
        sjAvail = b.getLong("sjAvail", 0);
        idleTime = b.getLong("idleTime", 0);
        deviceID = b.getString("deviceID");
        if (deviceID == null) deviceID = "";
    }

    public void writeToParcel(Parcel out, int flags) {
        Bundle b = new Bundle();
        b.putString("startTime", startTime);
        b.putString("lastTime", lastTime);
        b.putInt("totCharge", totCharge);
        b.putInt("totDischarge", totDischarge);
        b.putLong("lastPoint", lastPoint);
        b.putInt("totCap", totCap);
        b.putLong("sjBurn", sjBurn);
        b.putLong("sjAvail", sjAvail);
        b.putLong("idleTime", idleTime);
        b.putString("deviceID", deviceID);
        out.writeBundle(b);
    }

    public static final Parcelable.Creator<MatchBoxModel> CREATOR = new Parcelable.Creator<MatchBoxModel>() {
        public MatchBoxModel createFromParcel(Parcel in) {
            return new MatchBoxModel(in);
        }

        public MatchBoxModel[] newArray(int size) {
            return new MatchBoxModel[size];
        }
    };

    protected String getStartTime() {
        return(startTime);
    }

    protected void setStartTime(String sTime) {
        startTime = sTime;
    }

    protected String getLastTime() {
        return(lastTime);
    }

    protected void setLastTime(String lTime) {
        lastTime = lTime;
    }

    protected String getDeviceID() {
        return(deviceID);
    }

    protected void setDeviceID(String id) {
        deviceID = id;
    }

    protected int getTotCharge() {
        return(totCharge);
    }

    protected int getTotDischarge() {
        return(totDischarge);
    }

    protected long getLastPoint() {

        return(lastPoint);
    }

    protected int getCapacity() {
        if (totCap < 1) totCap = DEFAULT_CAPACITY;
        return(totCap);
    }

    protected long getUsedSJ() {
        return(sjBurn);
    }

    protected long getAvailableSJ() {
        return(sjAvail);
    }

    protected long getIdleTime() {
        return(idleTime);
    }

    protected void setAvailableSJ(long val) {
        if (val >= 0) sjAvail = val;
    }

    protected void setUsedSJ(long val) {
        sjBurn = val;
    }

    protected void incTotCharge(long val) {
        totCharge += val;
    }

    protected void incTotDischarge(long val) {
        totDischarge += val;
    }

    protected void setIdleTime(long val) {
        idleTime = val;
    }

    protected void setTotCharge(int val) {
        if (val > totCharge) totCharge = val;
    }

    protected void setTotDischarge(int val) {
        if (val > totDischarge)  totDischarge = val;
    }

    protected void setlastPoint(long val) {
        lastPoint = val;
    }

    /*protected void setCapacity(int val) {
        if (val > totCap) totCap = val;
    }*/

    public String printTime() {
        Date d = new Date();
        return(String.format(d.toString()));
    }

    private int getBatteryCap() {
        File f = new File("/sys/class/power_supply/battery/charge_full_design");
        if (!f.exists()) f = new File("/sys/class/power_supply/battery/energy_full");
        if (!f.exists()) return(12);
        String totStr = readLine(f);
        try {
            if (totStr.length() > 4) totStr = totStr.substring(0, 4);
            return(Integer.parseInt(totStr)/200);
        } catch (Exception e) {
            Log.w("Veeyah", "Exception reading capacity: " + e.getClass() + ": " + e.getMessage());
            return(-1);
        }
    }

    private String readLine(File f) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line = br.readLine();
            if (line == null) line = "";
            br.close();
            return(line);
        } catch (Exception e) {
            Log.w("Veeyah", "Exception reading current: "+e.getClass()+": "+e.getMessage());
            return("");
        }
    }

    public int describeContents() {
        return 0;
    }
}
