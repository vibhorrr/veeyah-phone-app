package com.veeyah.measuretest;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

/**
 * This is the main controller for the Veeyah Battery Manager.
 *
 * Note that this class has two intents for the Activity
 * 1. Refresh View(MatchBoxModel) - called whenever the model updates based on the Battery BroadcastReceiver or communication with the server
 *                                - provides the latest data for the UI
 * 2. Get User Info() - called whenever the controller needs to get a deviceID
 *               - the register method should get firstName, lastName, and password info from the user and then invoke
 *               - the Register(firstName, lastName, password) action of this service
 *
 * It maintains the MatchBoxModel (saved in JSON via MatchBoxJSONSerializer)
 * and triggers communciation with the server using the MatchBoxService_Impl class
 * Created by Williams on 12/11/13.
 */
public class VeeyahBattery extends Service {

    private MatchBoxService_Impl mbs;
    private MatchBoxModel mbm;
    protected static LocalBroadcastManager lbm;
    private final IBinder mBinder = new VeeyahBinder();
    public final static String GET_USER_INFO = "com.veeyah.GetUserInfo";
    public final static String REFRESH = "com.veeyah.Refresh";
    public final static String REGISTER = "com.veeyah.Register";
    public final static String MB_MODEL = "mbm";
    public final static String USER_NAME = "name";
    public final static String USER_EMAIL = "email";
    public final static String PASSWORD = "pass";
    public final static int NOTE_ID = 3600;

    public VeeyahBattery() {
        super();
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        if (REGISTER.equals(intent.getAction())) {
            String name = intent.getStringExtra(USER_NAME);
            String email = intent.getStringExtra(USER_EMAIL);
            String pass = intent.getStringExtra(PASSWORD);
            doRegister(name, email, pass);
        } else if (REFRESH.equals(intent.getAction())) {
            refreshView();
        } else if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.i("Veeyah", "Started after reboot");
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    protected static void registerLocalBroadcast(BroadcastReceiver br, Context myContext) {
        if (lbm == null) lbm = LocalBroadcastManager.getInstance(myContext);
        lbm.registerReceiver(br, getIntentFilter());
    }

    protected static void unregister(BroadcastReceiver br) {
        if (lbm != null) {
            lbm.unregisterReceiver(br);
        }
    }

    private void init() {
        try {
            IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

            BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver(){
                @Override
                public void onReceive(Context context, Intent intent){
                    processBatteryInfo(intent);
                }
            };
            registerReceiver(batteryLevelReceiver, batteryLevelFilter);
            mbs = new MatchBoxService_Impl();
            loadData();
            refreshView();
        } catch (Exception e) {
            Log.w("Veeyah", "Exception initializing VeeyahBattery: "+e.getClass()+": "+e.getMessage());
        }
    }

    protected Notification getServiceNotification(MatchBoxModel mbm) {
        boolean hasSJ = (mbm.getAvailableSJ() > 0);
        String cStr = "You ran out of solar!";
        if (hasSJ) cStr = "You've got solar!";
        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        return(new NotificationCompat.Builder(this).setTicker("Solar Status")
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Veeyah Solar")
                .setContentText(cStr)
                .setContentIntent(pi)
                .setAutoCancel(false)
                .build());
    }

    private void loadData() {
        mbm = MatchBoxJSONSerializer.loadMbInfo(this);
        if (mbm == null) {
            mbm = new MatchBoxModel();
            Intent getInfo = new Intent(this, MainActivity.class);
            getInfo.setAction(GET_USER_INFO);
            if (lbm != null) lbm.sendBroadcast(getInfo);
        }
    }

    private void refreshView() {
        MatchBoxJSONSerializer.saveMbInfo(mbm, this);
        Intent refresh = new Intent(this, MainActivity.class);
        refresh.setAction(REFRESH);
        refresh.putExtra(MB_MODEL, mbm);
        if (lbm != null) lbm.sendBroadcast(refresh);
        startForeground(NOTE_ID, getServiceNotification(mbm));
    }

    protected void processBatteryInfo(Intent i) {
            loadData();
        try {
            mbm.setLastTime(mbm.printTime());
            long curPoint = i.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            long lastPoint = mbm.getLastPoint();
            int batteryStatus = i.getIntExtra(BatteryManager.EXTRA_STATUS,0);

            if (lastPoint <= 0) lastPoint = curPoint;
            long delta = lastPoint-curPoint;
            checkSunJoules(delta, mbm.getCapacity(), (batteryStatus == BatteryManager.BATTERY_STATUS_FULL));
            mbm.setlastPoint(curPoint);
        } catch (Exception e) {
            Log.w("Veeyah", "Exception processing battery info: "+e.getClass()+": "+e.getMessage());
        }
            refreshView();
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(REFRESH);
        filter.addAction(GET_USER_INFO);
        return(filter);
    }

    private boolean burnSunJoules(long l) {  //long l describes sunjoules in hundreths
        if (l > mbm.getAvailableSJ()) {
            mbm.setUsedSJ(mbm.getUsedSJ()+mbm.getAvailableSJ());
            mbm.setAvailableSJ(0);
            return(false);
        }
        mbm.setUsedSJ(mbm.getUsedSJ()+l);
        mbm.setAvailableSJ(mbm.getAvailableSJ()-l);
        return(true);
    }

    protected void registrationSuccess(String id) {
        mbm.setDeviceID(id);
        MatchBoxJSONSerializer.saveMbInfo(mbm, this);
    }

    private void doRegister(String name, String email, String password) {
        mbs.doRegister(this, name, email, password);
    }

    protected boolean checkSunJoules(long delta, int cap, boolean full) {
        //note delta will be negative whenever a charge occurs and positive whenever a discharge occurs
        long sj = mbm.getAvailableSJ();
        if (delta != 0 || !full) mbm.setIdleTime(0);
        if (sj <= 0) {  //no sun joules!  try to get some, increment stats, return false
            mbs.getSunJoules(100, this, mbm.getDeviceID());
            if (delta > 0) {
                mbm.incTotDischarge((long) Math.ceil(delta*cap*3.6));
            } else if (delta < 0) {
                mbm.incTotCharge((long) (-1*Math.floor(delta*cap*3.6)));
            }
            return(false);
        }
        if (delta > 0) {  //battery level decreased, update stats, burn sunjoules
            long toBurn = (long) Math.ceil(delta*cap*3.6);
            mbm.incTotDischarge(toBurn);
            if (burnSunJoules(toBurn)) return(true);
        } else if (delta < 0) {  //battery level increased, update stats, try to get more sunjoules
            long toBurn = (long) Math.floor(-1*delta*cap*3.6);
            mbm.incTotCharge(toBurn);
            if (sj < 1000) {
                mbs.getSunJoules((int) Math.ceil(100-sj/100), this, mbm.getDeviceID());
            }
        } else {  //battery level constant
            if (!full) return(true);
            if (mbm.getIdleTime() == 0) {  //the first time this has happened, note the time
                mbm.setIdleTime(System.currentTimeMillis());
                return(true);
            }
            long d = System.currentTimeMillis()-mbm.getIdleTime();
            if (d > 60*60*1000) {  //the level has not changed in an hour, reset the timer and burn a sunjoule
                mbm.setIdleTime(System.currentTimeMillis());
                if (burnSunJoules(100)) return(true);
                return(false);
            } else {
                return(true);
            }
        }
        return(false);
    }

    protected void addSunJoules(int sj) {
        Toast.makeText(this, "Recieved " + sj + " SunJoules.", Toast.LENGTH_LONG).show();
        mbm.setAvailableSJ(mbm.getAvailableSJ()+(sj*100));
        refreshView();
    }

    public class VeeyahBinder extends Binder {
        /*VeeyahBattery getService() {
            return VeeyahBattery.this;
        }*/
    }

    @Override
    public IBinder onBind(Intent bindIntent) {
        return(mBinder);
    }

    //this commented section notes a number of device-dependant places where battery info may reside
    /*private String readCurrent() {
        File f;

        f = new File("/sys/class/power_supply/usb/current_now");
        if (f.exists())
            return readLine(f);

        f = new File("/sys/class/power_supply/ac/current_now");
        if (f.exists())
            return readLine(f);

        f = new File("/sys/class/power_supply/battery/current_now");
        if (f.exists())
            return readLine(f);

        // htc desire hd / desire z / inspire?
        if (Build.MODEL.toLowerCase().contains("desire hd") ||
                Build.MODEL.toLowerCase().contains("desire z") ||
                Build.MODEL.toLowerCase().contains("inspire")) {

            f = new File("/sys/class/power_supply/battery/batt_current");
            if (f.exists()) {
                return readLine(f);
            }
        }

        // nexus one cyangoenmod
        f = new File("/sys/devices/platform/ds2784-battery/getcurrent");
        if (f.exists()) {
            return readLine(f);
        }

        // sony ericsson xperia x1
        f = new File("/sys/devices/platform/i2c-adapter/i2c-0/0-0036/power_supply/ds2746-battery/current_now");
        if (f.exists()) {
            return readLine(f);
        }

        // xdandroid
        //if (Build.MODEL.equalsIgnoreCase("MSM")) {
        f = new File("/sys/devices/platform/i2c-adapter/i2c-0/0-0036/power_supply/battery/current_now");
        if (f.exists()) {
            return readLine(f);
        }
        //}

        // droid eris
        f = new File("/sys/class/power_supply/battery/smem_text");
        if (f.exists()) {
            return readLine(f);
        }

        // htc sensation / evo 3d
        f = new File("/sys/class/power_supply/battery/batt_attr_text");
        if (f.exists())
        {
            return readLine(f);
        }

        // some htc devices
        f = new File("/sys/class/power_supply/battery/batt_current");
        if (f.exists())
            return readLine(f);

        // nexus one
        f = new File("/sys/class/power_supply/battery/current_now");
        if (f.exists())
            return readLine(f);

        // samsung galaxy vibrant
        f = new File("/sys/class/power_supply/battery/batt_chg_current");
        if (f.exists())
            return readLine(f);

        // sony ericsson x10
        f = new File("/sys/class/power_supply/battery/charger_current");
        if (f.exists())
            return readLine(f);

        // Nook Color
        f = new File("/sys/class/power_supply/max17042-0/current_now");
        if (f.exists())
            return readLine(f);

        return("0.0");
    }*/
}
